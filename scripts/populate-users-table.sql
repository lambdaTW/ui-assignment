CREATE TABLE IF NOT EXISTS users (
  acct TEXT PRIMARY KEY,
  pwd VARCHAR ( 64 ) NOT NULL,
  fullname TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

DROP TRIGGER IF EXISTS update_customer_modtime on users;

CREATE TRIGGER update_customer_modtime
BEFORE UPDATE ON users
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


/*
The password is bcrypt encrypted `TestPKN18test123`
*/
INSERT INTO
    users (acct, pwd, fullname)
VALUES
    ('acct1', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct1'),
    ('acct2', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct2'),
    ('acct3', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct3'),
    ('acct4', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct4'),
    ('acct5', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct5'),
    ('acct6', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct6'),
    ('acct7', '$2y$12$HfcHyi0FurtifIXjBdbIDerM5zEA7/fAZkj5K4fBakCrQkqCaolyi', 'acct7')
ON CONFLICT DO NOTHING;
