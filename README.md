# ui-assignment
ui.com python assignment

## Usage
### Run PostgreSQL
```shell-script
make pg-docker
```

### Populate users table
```shell-script
make populate-users-table
```
