pg-docker:
	docker run -d --name ui-pg --rm -e POSTGRES_USER=ui_test -e POSTGRES_HOST_AUTH_METHOD=true -p 0.0.0.0:5432:5432 postgres:alpine

populate-users-table:
	psql -h 127.0.0.1 -p 5432 -U ui_test ui_test < scripts/populate-users-table.sql
